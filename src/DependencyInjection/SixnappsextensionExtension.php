<?php
	
	namespace SixnappsExtensionBundle\DependencyInjection;
	
	use Symfony\Component\Config\FileLocator;
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
	use Symfony\Component\HttpKernel\DependencyInjection\Extension;
	
	/**
	 * Class SixnappsExtensionExtension
	 *
	 * @package Sixnapps\AnalyticBundle\DependencyInjection
	 */
	class SixnappsextensionExtension extends Extension
	{
		/**
		 * @param array            $configs
		 * @param ContainerBuilder $container
		 *
		 * @throws \Exception
		 */
		public function load( array $configs, ContainerBuilder $container )
		{
			$config = $this->processConfiguration( new Configuration(), $configs );
			$loader = new YamlFileLoader( $container, new FileLocator( __DIR__ . '/../Resources/config' ) );
			$loader->load( 'services.yaml' );
			$container->setParameter( 'sixnapps_extension', $config );
		}
		
		
		/**
		 * @return string
		 */
		public function getAlias()
		{
			return 'sixnapps_extension';
		}
		
		
		/**
		 * @return string
		 */
		public function getNamespace()
		{
//			return 'http://helios-ag.github.io/schema/dic/fm_summernote';
			return 'sixnapps_extension';
		}
	}
