<?php

	namespace Sixnapps\ExtensionBundle\DependencyInjection;

	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;

	class Configuration implements ConfigurationInterface
	{
		/**
		 * @return TreeBuilder
		 */
		public function getConfigTreeBuilder()
		{
			$treeBuilder = new TreeBuilder('sixnapps_extension' );
			// Keep compatibility with symfony/config < 4.2
			if (!method_exists($treeBuilder, 'getRootNode')) {
				$treeBuilder->root( 'sixnapps_extension' );
			} else {
				$treeBuilder->getRootNode();
			}
			
			return $treeBuilder;
		}
	}
