<?php

	namespace Sixnapps\ExtensionBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;

	/**
	 * Class AppServices
	 *
	 * @package Sixnapps\ExtensionBundle\Services
	 */
	class AppServices
	{

		/**
		 * @var EntityManagerInterface
		 */
		private $em;


		/**
		 * AppServices constructor.
		 *
		 * @param EntityManagerInterface $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}


		/**
		 * @param int $longueur
		 *
		 * @return string
		 */
		public function genererMDP( $longueur = 8 )
		{
			$mdp         = "";
			$possible    = "34679ACDEFGHJKLMNPQRTVWXY";
			$longueurMax = strlen( $possible );
			if ( $longueur > $longueurMax ) {
				$longueur = $longueurMax;
			}
			$i = 0;
			while ( $i < $longueur ) {
				$caractere = substr( $possible, mt_rand( 0, $longueurMax - 1 ), 1 );

				if ( !strstr( $mdp, $caractere ) ) {
					$mdp .= $caractere;
					$i++;
				}
			}
			return $mdp;
		}


		/**
		 * @param $data
		 *
		 * @return mixed
		 */
		public function toURL( $data )
		{
			$data = str_replace( [ '#', '?', '%', ':', '«', '»', '!', '>', '<', '=', '"', '.', ',', '(', ')' ], '', $data );
			$data = str_replace( [ '&eacute;', 'é', 'è', 'ê', 'ë', 'É', 'Ê', 'Ë' ], "e", $data );
			$data = str_replace( [ 'î', 'ï', 'Î', 'Ï' ], 'i', $data );
			$data = str_replace( [ 'û', 'ù', 'ü', 'Û', 'Ù', 'Ü' ], 'u', $data );
			$data = str_replace( [ 'à', 'â', 'ä', 'À', 'Â', 'Ä' ], 'a', $data );
			$data = str_replace( [ 'ô', 'ö', 'Ô', 'Ö' ], 'o', $data );
			$data = str_replace( 'ç', 'c', $data );
			$data = str_replace( [ '&amp;', '&' ], '-et-', $data );
			$data = str_replace( [ "'", "/" ], "-", $data );
			$data = strtolower( $data );
			$data = trim( $data );
			$data = str_replace( '-', ' ', $data );
			$data = preg_replace( '/\s{2,}/', ' ', $data );
			$data = str_replace( " ", "-", $data );
			$data = strip_tags( $data );

			return $data;
		}


		/**
		 * @param $nombre
		 *
		 * @return string
		 */
		public function convertNumberToString( $nombre )
		{
			$nombre = str_replace(' ', '', $nombre);
			$nombre = sprintf( "%012.2f", $nombre );

			$nb1 = [ 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf' ];

			$nb2 = [ 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante', 'quatre-vingt', 'quatre-vingt' ];

			# Décomposition du chiffre
			# Séparation du nombre entier et des décimales
			if ( preg_match( "/\b,\b/i", $nombre ) ) {
				$nombre = explode( ',', $nombre );
			} else {
				$nombre = explode( '.', $nombre );
			}
			$nmb = $nombre[ 0 ];

			# Décomposition du nombre entier par tranche de 3 nombre (centaine, dizaine, unitaire)
			$i = 0;
			while ( strlen( $nmb ) > 0 ) {
				$nbtmp[ $i ] = substr( $nmb, -3 );
				if ( strlen( $nmb ) > 3 ) {
					$nmb = substr( $nmb, 0, strlen( $nmb ) - 3 );
				} else {
					$nmb = '';
				}
				$i++;
			}

			$nblet = '';
			## Taitement du côté entier
			for ( $i = 1; $i >= 0; $i-- ) {

				if ( strlen( trim( $nbtmp[ $i ] ) ) == 3 ) {

					$ntmp = substr( $nbtmp[ $i ], 1 );
					if ( substr( $nbtmp[ $i ], 0, 1 ) <> 1 && substr( $nbtmp[ $i ], 0, 1 ) <> 0 ) {
						$nblet .= $nb1[ substr( $nbtmp[ $i ], 0, 1 ) - 1 ];
						if ( $ntmp <> 0 ) {
							$nblet .= ' cent ';
						} else {
							$nblet .= ' cents ';
						}
					} elseif ( substr( $nbtmp[ $i ], 0, 1 ) <> 0 ) {
						$nblet .= 'cent ';
					}

				} else {
					$ntmp = $nbtmp[ $i ];
				}
				if ( $ntmp > 0 && $ntmp < 20 ) {
					if ( !( $i == 1 && $nbtmp[ $i ] == 1 ) ) {
						$nblet .= $nb1[ $ntmp - 1 ] . ' ';
					}
				}

				if ( $ntmp >= 20 && $ntmp < 60 ) {
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 1 :
							$sep = ' et ';
							break;
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}

					if (substr( $ntmp, 1, 1 ) - 1 < 0){
						$_nb1 = '';
					} else {
						$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
					}
					$nblet .= $nb2[ substr( $ntmp, 0, 1 ) - 2 ] . $sep . $_nb1 . ' ';
				}

				if ( $ntmp >= 60 && $ntmp < 80 ) {
					$nblet .= $nb2[ 4 ];
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 1 :
							$sep = ' et ';
							break;
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}

					if ( substr( $ntmp, 0, 1 ) <> 7 ) {

						if (substr( $ntmp, 1, 1 ) - 1 < 0){
							$_nb1 = '';
						} else {
							$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
						}

						$nblet .= $sep . $_nb1. ' ';
					} else {
						if ( substr( $ntmp, 1, 1 ) + 9 == 9 )
							$sep = '-';
						$nblet .= $sep . $nb1[ substr( $ntmp, 1, 1 ) + 9 ] . ' ';
					}

				}

				if ( $ntmp >= 80 && $ntmp < 100 ) {
					$nblet .= $nb2[ 6 ];
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}

					if ( substr( $ntmp, 0, 1 ) <> 9 ) {
						if (substr( $ntmp, 1, 1 ) - 1 < 0){
							$_nb1 = '';
						} else {
							$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
						}

						$nblet .= $sep . $_nb1;
						if ( substr( $ntmp, 1, 1 ) == 0 )
							$nblet .= 's';
					} else {
						if ( substr( $ntmp, 1, 1 ) == 0 )
							$sep = '-';
						$nblet .= $sep . $nb1[ substr( $ntmp, 1, 1 ) + 9 ];
					}
					$nblet .= ' ';
				}

				if ( $i == 1 && $nbtmp[ $i ] <> 0 ) {
					$nblet .= 'mille ';
				}

			}

			if ( $nombre[ 0 ] > 1 )
				$nblet .= 'euros ';
			if ( $nombre[ 0 ] == 1 )
				$nblet .= 'euro ';

			## Traitement du côté décimale
			if ( $nombre[ 0 ] > 0 && $nombre[ 1 ] > 0 )
				$nblet .= ' et ';
			$ntmp = substr( $nombre[ 1 ], 0, 2 );
			if ( !empty( $ntmp ) ) {
				if ( $ntmp > 0 && $ntmp < 20 ) {
					$nblet .= $nb1[ $ntmp - 1 ] . ' ';
				}

				if ( $ntmp >= 20 && $ntmp < 60 ) {
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 1 :
							$sep = ' et ';
							break;
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}
					if (substr( $ntmp, 1, 1 ) - 1 < 0){
						$_nb1 = '';
					} else {
						$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
					}
					$nblet .= $nb2[ substr( $ntmp, 0, 1 ) - 2 ] . $sep . $_nb1. ' ';
				}

				if ( $ntmp >= 60 && $ntmp < 80 ) {
					$nblet .= $nb2[ 4 ];
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 1 :
							$sep = ' et ';
							break;
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}

					if ( substr( $ntmp, 0, 1 ) <> 7 ) {
						if (substr( $ntmp, 1, 1 ) - 1 < 0){
							$_nb1 = '';
						} else {
							$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
						}
						$nblet .= $sep . $_nb1 . ' ';
					} else {
						if ( substr( $ntmp, 1, 1 ) + 9 == 9 )
							$sep = '-';
						$nblet .= $sep . $nb1[ substr( $ntmp, 1, 1 ) + 9 ] . ' ';
					}

				}

				if ( $ntmp >= 80 && $ntmp < 100 ) {
					$nblet .= $nb2[ 6 ];
					switch ( substr( $ntmp, 1, 1 ) ) {
						case 0 :
							$sep = '';
							break;
						default:
							$sep = '-';
					}

					if ( substr( $ntmp, 0, 1 ) <> 9 ) {
						if (substr( $ntmp, 1, 1 ) - 1 < 0){
							$_nb1 = '';
						} else {
							$_nb1 = $nb1[ substr( $ntmp, 1, 1 ) - 1 ];
						}
						$nblet .= $sep . $_nb1;
						if ( substr( $ntmp, 1, 1 ) == 0 )
							$nblet .= 's';
					} else {
						if ( substr( $ntmp, 1, 1 ) == 0 )
							$sep = '-';
						$nblet .= $sep . $nb1[ substr( $ntmp, 1, 1 ) + 9 ];
					}
					$nblet .= ' ';

				}

				if ( $ntmp <> 0 && !empty( $ntmp ) ) {
					if ( $ntmp > 1 ) {
						$nblet .= 'centimes ';
					} else {
						$nblet .= 'centime ';
					}
				}
			}

			return trim( str_replace( '  ', ' ', $nblet ) );

		}
	}
