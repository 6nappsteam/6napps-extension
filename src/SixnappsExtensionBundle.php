<?php
	namespace Sixnapps\ExtensionBundle;
	
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\HttpKernel\Bundle\Bundle;
	
	/**
	 * Class SixnappsExtensionBundle
	 *
	 * @package Sixnapps\ExtensionBundle
	 */
	class SixnappsExtensionBundle extends Bundle
	{
		/**
		 * @param ContainerBuilder $container
		 */
		public function build( ContainerBuilder $container )
		{
			parent::build( $container );
		}
	}
