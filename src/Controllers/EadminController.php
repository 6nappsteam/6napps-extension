<?php

	namespace Sixnapps\ExtensionBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
	use Symfony\Component\Filesystem\Filesystem;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class EadminController
	 *
	 * @package Sixnapps\ExtensionBundle\Controllers
	 */
	class EadminController extends AbstractController
	{


		/**
		 * @return Response
		 */
		public function parameters()
		{
			return $this->render( '@SixnappsExtension/parameters/edit.html.twig', [
			] );

		}


		/**
		 * @return JsonResponse
		 */
		public function switchMaintenance()
		{
			$fs = new Filesystem();
			try {
//            Check si le fichier lock exist pour soit passer en mode maintenance ou le désactiver
				if ( $fs->exists( $this->get('kernel')->getProjectDir() . '/var/cache/lock' ) ) {
					$fs->remove( $this->get('kernel')->getProjectDir() . '/var/cache/lock' );
					return new JsonResponse(
						[
							'success' => TRUE,
							'statut'  => FALSE,
							'message' => "Désactivation du mode maintenance efféctué",
						]
					);
				}
				else {
					mkdir( $this->get('kernel')->getProjectDir() . '/var/cache/lock' );
					return new JsonResponse(
						[
							'success' => TRUE,
							'statut'  => TRUE,
							'message' => "Passage en mode maintenance efféctué",
						]
					);
				}

			}
			catch ( IOExceptionInterface $exception ) {
				return new JsonResponse(
					[
						'success' => FALSE,
						'statut'  => TRUE,
						'message' => "L'opération à échoué",
					]
				);
			}
		}

	}
