<?php

	namespace Sixnapps\ExtensionBundle\Controllers;

	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class FeaturesController
	 *
	 * @package Sixnapps\ExtensionBundle\Controllers
	 */
	class FeaturesController extends AbstractController
	{
		/**
		 * @return Response
		 */
		public function show()
		{

			$form = $this->createFormBuilder( [] );
			$form->add( 'name', TextType::class )
				 ->add( 'email', EmailType::class )
				 ->add( 'message', TextareaType::class )
			;
			if ( array_key_exists( 'VichUploaderBundle', $this->container->get( 'kernel' )->getBundles() ) ) {
				$form->add( 'pdf', \Vich\UploaderBundle\Form\Type\VichFileType::class, [
					'required'       => FALSE,
					'allow_delete'   => FALSE,
					'download_label' => 'download_file',
				] );
			}
			$form->add( 'send', SubmitType::class );

			return $this->render( '@SixnappsExtension/features/show.html.twig', [
				'Form' => $form->getForm()->createView(),
			] );
		}
	}
