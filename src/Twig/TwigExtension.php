<?php
	
	namespace Sixnapps\ExtensionBundle\Twig;
	
	use Doctrine\ORM\EntityManagerInterface;
	use Sixnapps\ExtensionBundle\Services\AppServices;
	use Symfony\Component\HttpFoundation\RequestStack;
	use Symfony\Component\HttpKernel\KernelInterface;
	
	/**
	 * Class TwigExtension
	 *
	 * @package AppBundle\Twig
	 */
	class TwigExtension extends \Twig_Extension
	{
		/**
		 * @var
		 */
		private $em;
		
		/**
		 * @var KernelInterface
		 */
		private $kernel;
		
		/**
		 * @var AppServices
		 */
		private $appServices;
		
		/**
		 * @var RequestStack
		 */
		private $requestStack;
		
		
		/**
		 * TwigExtension constructor.
		 *
		 * @param KernelInterface        $kernel
		 * @param EntityManagerInterface $em
		 * @param RequestStack           $requestStack
		 * @param AppServices            $appServices
		 */
		public function __construct( KernelInterface $kernel, EntityManagerInterface $em, RequestStack $requestStack, AppServices $appServices )
		{
			$this->kernel       = $kernel;
			$this->em           = $em;
			$this->requestStack = $requestStack;
			$this->appServices  = $appServices;
			
		}
		
		
		/**
		 * @return array|\Twig_Filter[]
		 */
		public function getFilters()
		{
			return [
				new \Twig_SimpleFilter( 'toURL', [ $this, 'toURL' ] ),
				new \Twig_SimpleFilter( 'dbl', [ $this, 'dbl' ] ),
				new \Twig_SimpleFilter( 'dblz', [ $this, 'dblz' ] ),
				new \Twig_SimpleFilter( 'array_unique', [ $this, 'array_unique' ] ),
				new \Twig_SimpleFilter( 'html', [ $this, 'html' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFilter( 'md5', [ $this, 'md5' ] ),
				new \Twig_SimpleFilter( 'ellipsis', [ $this, 'ellipsis' ] ),
				new \Twig_SimpleFilter( 'percentage', [ $this, 'percentage' ] ),
				new \Twig_SimpleFilter( 'basename', [ $this, 'basenameFilter' ] ),
			];
		}
		
		
		/**
		 * @var string $value
		 * @return string
		 */
		public function basenameFilter( $value, $suffix = '' )
		{
			return basename( $value, $suffix );
		}
		
		
		/**
		 * @param $value
		 *
		 * @return mixed
		 */
		public function percentage( $value )
		{
			return $value * 100;
		}
		
		
		/**
		 * @param $string
		 *
		 * @return mixed
		 */
		public function html( $string )
		{
			return $string;
		}
		
		
		/**
		 * @param $data
		 * @param $size
		 *
		 * @return string
		 */
		public function ellipsis( $data, $size )
		{
			$dataCleaned = strip_tags( $data );
			
			if ( strlen( $dataCleaned ) > $size ) {
				$dataEllipsed = substr( $dataCleaned, 0, $size ) . ' …';
			} else {
				$dataEllipsed = $dataCleaned;
			}
			return $dataEllipsed;
		}
		
		
		/**
		 * @param $data
		 *
		 * @return string
		 */
		public function md5( $data )
		{
			return md5( $data );
		}
		
		
		/**
		 * @param $array
		 *
		 * @return array
		 */
		public function array_unique( $array )
		{
			return array_unique( $array );
		}
		
		
		/**
		 * @param $data
		 *
		 * @return mixed
		 */
		public function toURL( $data )
		{
			return $this->appServices->toURL( $data );
		}
		
		
		/**
		 * @param $float
		 *
		 * @return string
		 */
		public function dbl( $float )
		{
			if ( $float == '' )
				return '';
			return number_format( $float, 2, '.', '' );
		}
		
		
		/**
		 * @param $float
		 *
		 * @return string
		 */
		public function dblz( $float )
		{
			if ( $float == '' )
				return '0.00';
			return number_format( $float, 2, '.', '' );
		}
		
		
		/**
		 * @return array|\Twig_Function[]
		 */
		public function getFunctions()
		{
			return [
				new \Twig_SimpleFunction( 'bundleExists', [ $this, 'bundleExists' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'flashbag', [ $this, 'flashbag' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'json_decode', [ $this, 'jsonDecode' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'json_encode', [ $this, 'jsonEncode' ], [ 'is_safe' => [ 'html' ] ] ),
			];
		}
		
		
		/**
		 * @param $bundle
		 *
		 * @return bool
		 */
		public function bundleExists( $bundle )
		{
			return array_key_exists(
				$bundle,
				$this->kernel->getBundles()
			);
		}
		
		
		/**
		 * @param $messages
		 *
		 * @return string
		 */
		public function flashbag( $messages )
		{
			$result = '';
			foreach ( $messages as $label => $flashes ) {
				foreach ( $flashes as $flash ) {
					$result .= '<div class="uk-alert uk-alert-' . $label . '" data-uk-alert="">' .
							   '<a href="#" class="uk-alert-close uk-close"></a>' .
							   $flash .
							   '</div>';
				}
			}
			return $result;
		}
		
		
		/**
		 * @param $str
		 *
		 * @return mixed
		 */
		public function jsonDecode( $str )
		{
			return json_decode( $str );
		}
		
		
		/**
		 * @param $str
		 *
		 * @return false|string
		 */
		public function jsonEncode( $str )
		{
			return json_encode( $str );
		}
	}
